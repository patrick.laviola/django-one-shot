from django.shortcuts import get_object_or_404, render, redirect
from .models import TodoList
from .forms import TodoListForm


def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail_view(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {"todo_list": todo_list}
    return render(request, "todos/todo_list_detail.html", context)


# def show_model_name(request, id):
#   model_instance = ModelName.objects.get(id=id)
#   context = {
#     "model_instance": model_instance,
#   }
#   return render(request, "model_names/detail.html", context)


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = TodoListForm()

    context = {"form": form}

    return render(request, "todos/create_todo_list.html", context)


def update_todo_list(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            updated_list = form.save()
            return redirect("todo_list_detail", id=updated_list.id)
    else:
        form = TodoListForm(instance=todo_list)

    context = {"form": form}

    return render(request, "todos/update_todo_list.html", context)
