from django.urls import path
from . import views

urlpatterns = [
    path("", views.todo_list_view, name="todo_list_list"),
    path("<int:id>/", views.todo_list_detail_view, name="todo_list_detail"),
    path("<int:id>/edit/", views.update_todo_list, name="todo_list_update"),
    path("create/", views.create_todo_list, name="todo_list_create"),
]
